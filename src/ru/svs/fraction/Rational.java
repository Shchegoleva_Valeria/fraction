package ru.svs.fraction;

/**
 * Класс для вычисления действий с дробями.
 *
 * @author Щеголева В., группа 15/18
 */

public class Rational {
    private int num;
    private int denom;

    public Rational(int num, int denom) {
        this.num = num;
        this.denom = denom;
    }

    public Rational() {
        this(1, 1);
    }

    public Rational(int num) {
        this.num = 1;
    }

    @Override
    public String toString() {
        return num + "/" + denom;
    }


    /**
     * Метод add служит для вычисления суммы двух дробей.
     *
     * @param rational1
     * @return объект
     */
    public Rational add(Rational rational1) {
        int x;
        int y;
        if (this.denom == rational1.denom) {
            x = this.num + rational1.num;
            y = denom;
        } else {
            x = this.denom * rational1.denom;
            y = this.num * rational1.denom + rational1.num * this.denom;
        }
        return new Rational(x, y);
    }


    /**
     * Метод subtract служит для вычисления разности двух дробей.
     *
     * @param rational1
     * @return объект
     */
    public Rational subtract(Rational rational1) {
        int x;
        int y;
        if (this.denom == rational1.denom) {
            x = this.num - rational1.num;
            y = denom;
        } else {
            x = this.denom * rational1.denom;
            y = this.num * rational1.denom - rational1.num * this.denom;
        }
        return new Rational(x, y);
    }


    /**
     * Метод multiply служит для вычисления произведения двух дробей.
     *
     * @param rational1
     * @return объект
     */
    public Rational multiply(Rational rational1) {
        int y = this.denom * rational1.denom;
        int x = this.num * rational1.num;
        return new Rational(x, y);
    }


    /**
     * Метод div служит для вычисления частного двух дробей.
     *
     * @param rational1
     * @return объект
     */
    public Rational div(Rational rational1) {
        int x = this.num * rational1.denom;
        int y = this.denom * rational1.num;
        return new Rational(x, y);
    }


}
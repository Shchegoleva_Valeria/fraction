package ru.svs.fraction;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Класс для демонстрации работы класса Rational.
 *
 * @author Щеголева В., группа 15/18
 */

public class Demo {

    static Scanner scan = new Scanner(System.in);

    public static void main(String[] args) {
        String str = input();

        String[] arr = str.split(" ");
        String n1 = arr[0];
        String[] first = n1.split("/");
        int numArr[] = new int[first.length];
        for (int i = 0; i < first.length; i++) {
            numArr[i] = Integer.parseInt(first[i]);
        }

        String n2 = arr[2];
        String[] second = n2.split("/");
        int num2Arr[] = new int[second.length];
        for (int i = 0; i < second.length; i++) {
            num2Arr[i] = Integer.parseInt(second[i]);
        }

        int num1 = numArr[0];
        int denom1 = numArr[1];
        int num2 = num2Arr[0];
        int denom2 = num2Arr[1];


        Rational rational1 = new Rational(num2, denom2);
        Rational rational = new Rational(num1, denom1);


        if (arr[1].equals("+")) {
            System.out.println("Результат сложения: " + rational.add(rational1));
        }
        if (arr[1].equals("-")) {
            System.out.println("Результат вычитания: " + rational.subtract(rational1));
        }
        if (arr[1].equals(":")) {
            System.out.println("Результат деления: " + rational.div(rational1));
        }
        if (arr[1].equals("*")) {
            System.out.println("Результат умножения: " + rational.multiply(rational1));
        }
    }

    public static boolean isCheck(String str) {
        Pattern pattern = Pattern.compile("[-]*[1-9][0-9]*/[-]*[1-9][0-9]*\\s[*:+-]\\s[-]*[1-9][0-9]*/[-]*[1-9][0-9]*");
        Matcher matcher = pattern.matcher(str);
        return matcher.find();
    }

    private static String input() {
        String str;
        boolean checkString;
        do {
            System.out.println("Введите исходные данные: ");
            str = scan.nextLine();
            checkString = isCheck(str);
            if (!checkString) {
                System.out.println("Некорректно введены данные");
            }
        } while (!checkString);
        return str;
    }


}

